#!/bin/bash
clear
echo "__________________________________________________________"
echo "              QeranaFramework INSTALLATION                "
echo "__________________________________________________________"
echo " "
read -p "Do you want to install QeranaFramework? (y/n)? " answer
current_path=`pwd`
echo " "
case $answer in
    y )
        echo  "       **Starting installation proccess**      "
        echo " "
        echo -e "--1)Please enter your project name: "
        read name
        echo ".............Cloning into $current_path/$name..................."
        echo " "
        git clone https://gitlab.com/tykson/qerana-core $name
        cd $current_path
        cd $name
        rm -fR .git
        echo "...........................Cloning(done)........................"
        echo " "
        read -p "* Do you want to upload into a git repository? (y/n)? " answer_git
            
            case $answer_git in
                y )
                    echo -e "--2.1)Please enter your git url: "
                    read url
                    git init
                    git remote add origin $url
                    git add .
                    git commit -m "Initial QFW commit"
                    git push origin master
                    git update-index --assume-unchanged _data_/logs/logs.log
                    git update-index --assume-unchanged _data_/logs/
                    git update-index --assume-unchanged _data_/files/
                    git remote add origin-qerana https://gitlab.com/tykson/qerana-core.git
                   
                    echo "--Repository updated"
                    ;;
                n )
                    echo "Finish"
                    ;;
             esac
        chmod +x cliqer
        chmod +x get
        
         echo  "*******FINISH INSTALLATION PROCESS*********"
        
    ;;
    n )
        echo "----Bye----"
        exit;
	
    ;;
esac

